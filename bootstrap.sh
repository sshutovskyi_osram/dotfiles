#!/bin/sh

#install vim
mkdir -p ~/.vim/bundle
mkdir -p ~/.vim/swapfiles
git clone https://github.com/VundleVim/Vundle.vim.git ~/.vim/bundle/Vundle.vim

ln -s ~/dotfiles/.vimrc ~/.vimrc
vim -c ':PluginInstall'

#install zsh
sh -c "$(curl -fsSL https://raw.github.com/robbyrussell/oh-my-zsh/master/tools/install.sh)"
ln -s ~/dotfiles/.zshrc ~/.zshrc
